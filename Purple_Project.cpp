// Author: Purple group (Dustin, Alberto, Andrew S., Tyler S.)
// Language: C++
// Date: 04/22/2014
// Purpose: Take input file of student's test grades, drop lowest grade, print grades with averages.

//supplied code !--
#include<iostream>
#include<fstream>
#include<string>
using namespace std;
const int NROWS =10;//Number of rows in the file
const int NCOLS =5;//Number of columns containing the test scores


int ReadTxtFile(ifstream &file, string name[], double test[][NCOLS], int ncolUsed=NCOLS);
//Function reads data from a tab delimited file, whose first column is a string and the others are 
//double or int. The function returns the number of rows read. The requires three parameters to be 
//passed to it. The fourth parameter is has a default value of NCOLS.

void outPutData(string header, string name[], double test[][NCOLS], int nDataPts, int ncolUsed=NCOLS);
//The function prints out the data read from the file. The function requires four parameters to be 
//passed to it. Does not return any value.

//end supplied --!

void theProgram();
//Function to output the purpose and the team. Returns no value.

void restOfMain(string name[], double test[][NCOLS], string& header);
//Function to continue the program with the assigned statements.
//Takes non-global variables and returns nothing.

void dropGrade(double test[][NCOLS], int currentRow);
//Function takes the current row and the array of doubles to find the lowest grade and drop it.
//Returns nothing

double testAverage(double test[][NCOLS], int currentRow);
//Function that takes the average of the test grades and finds the percentage
//Returns the percentage

string letterGrade(double);
//Function that takes the percentage grade and turns it into a letter grade.

void spitNewData(string name[], double test[][NCOLS], string& header);
//Function to output the new data. Takes in the data points and is used for printing the information. 
//Returns nothing.

int main()
{
   theProgram();  //Calls the information function.

   //supplied code !--
   string name[NROWS];
   string header, filename;
   double test[NROWS][NCOLS];
   char next;
   int nDataRows;
   ifstream file; //Declares file as an input file stream 

   cout<<"Please give me the filenames containing the test scores\n";
   getline(cin,filename);

//Opens the file and checks if the file has opened correctly
 
   file.open(filename);
   if(file.fail()){
     cout<<"Failed to open input file "<<filename<<endl;
     exit(1);
   }
   else{
      cout<<filename<<" successfully opened for reading\n";
   }
   getline(file,header);// Reads the column headers as a string
   nDataRows=ReadTxtFile(file, name, test); // Calls the function to read the file
   
   file.close();
       
   outPutData(header,name, test, nDataRows); //Calls the function to output the data read from the file
   cout<< "\n\nNumber of records in the file is "<<nDataRows<<endl;
   getchar();
   //end supplied code --!

   restOfMain(name, test, header); //Calls the student defined functions, from a function. Functception?

} //End of main




//supplied code !--
void outPutData( string header, string name[], double test[][NCOLS], int nDataRows, int ncolUsed)
{
   cout<<"\t"<<header<<endl;
   for(int i=0; i<nDataRows; i++){
   cout<<i<<"\t"<<name[i]<<"\t";
   for(int j=0; j<ncolUsed; j++){
      cout<<test[i][j]<<"\t";
   }
   cout<<endl;
   }

} 

int ReadTxtFile(ifstream &file, string name[], double test[][NCOLS], int ncolUsed)
{
   int i=0;
   char next;
   while (!file.eof()) //repeat until end of file
   {
   file>>name[i];
   for(int j=0; j<ncolUsed; j++){
       file>>test[i][j];
   }
   file.get(next);
   if(!file.eof()){
      file.putback(next);
   }
   i++;
  }
   return(i-1);
}
//end supplied code --!


void theProgram()
{
   cout << "\nWelcome to Grade-Finder v0.1.478b. Group Purple would like you to enjoy this\n"
   << "program. This program is designed to take input of a file, drop the lowest test\n"
   << "grade and output the average of the remaining tests in grade format.\n\n";
}

string letterGrade(double avgPercent)
{
   avgPercent = avgPercent * 10; //Since we didn't pass the percent, just the average.
   if(avgPercent >= 95)
   { 
      return "A+"; 
   }    
   else if ((avgPercent<95) && (avgPercent >=90))
   { 
      return "A"; 
   }
   else if ((avgPercent<90) && (avgPercent >=85))
   { 
      return "B+"; 
   } 
   else if ((avgPercent<85) && (avgPercent >=80))
   { 
      return "B"; 
   }
   else if ((avgPercent<80) && (avgPercent >=75))
   { 
      return "C+"; 
   }
   else if ((avgPercent<75) && (avgPercent >=70))
   { 
      return "C"; 
   }
   else if ((avgPercent<70) && (avgPercent >=65))
   { 
      return "D+"; 
   }
   else if ((avgPercent<65) && (avgPercent >=60))
   { 
      return "D"; 
   }
   else if (avgPercent<60)
   { 
      return "F"; 
   } 
}

void restOfMain(string name[], double test[][NCOLS], string& header)
{
   for (int currentRow = 0; currentRow < NROWS; currentRow++) //Loop through students and drop lowest grade
   {
      dropGrade(test, currentRow); //Calls the function to find and drop lowest grade
   }
   spitNewData(name, test, header); //Output function
}

void dropGrade(double test[][NCOLS], int currentRow)
{
   int lowestIndex = 0;
   for (int i = 1; i < NCOLS; i++) //loop through columns to compare grades.
   {
      if (test[currentRow][lowestIndex] > test[currentRow][i])
      {
         lowestIndex = i;
      }
   }
   test[currentRow][lowestIndex] = -1; //Sets dropped grade to -1 to distinguish from valid grades
}

void spitNewData(string name[], double test[][NCOLS], string& header)
{
   double temp = 0.0;
   header = header + "\tTest Avg Test %\tGrade"; //Append other categories to header
   cout << header << endl;
   for(int i = 0; i < NROWS; i++) //Loop through students
   {
      cout << name[i] << "\t";
      for(int j=0; j < NCOLS; j++) //Loop through grades
      {
         if (test[i][j] < 0) //Checks if grade is valid or dropped
         {
            cout << " \t";
         }
         else
         {
            cout << test[i][j] << "\t";
         }
      }
      temp = testAverage(test,i); //temp variable for ease of use
      cout << temp << "\t " << temp * 10 << "%\t" << letterGrade(temp) << endl; //outputs the final student data
   }
}

double testAverage(double test[][NCOLS], int currentRow)
{
   double average = 0;

   //Loop through the grades create the average
   for (int i = 0; i < NCOLS; i++) 
   {
      average += test[currentRow][i]; //sums test grades for the average
   }
   average = (average + 1) / (NCOLS - 1); 
   //Adds one to offset the -1 limiter for dropped grade then divides by the original number of columns minus 1 
   //to offset amount of tests
   return average;
}