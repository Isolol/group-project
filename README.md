group-project
=============

C101 Group Project

You are required to write a program that will help the faculty to analyze the test scores and 
assign grades. The program should be capable of reading a tab delimited file that contains 
the raw score for different tests. Your program should drop the lowest score and calculate 
the average percentage of the other tests. The program should then assign grades based on 
the following grading scheme. 

```c++
if(Average Pecentage >= 95){
 YourLetterGrade="A+";
 }else if ((Average Pecentage<95) && (Average Pecentage >=90)){
 YourLetterGrade="A"; 
 }else if ((Average Pecentage<90) && (Average Pecentage >=85)){
 YourLetterGrade="B+"; 
 }else if ((Average Pecentage<85) && (Average Pecentage >=80)){ 
 YourLetterGrade="B"; 
 }else if ((Average Pecentage<80) && (Average Pecentage >=75)){ 
 YourLetterGrade="C+"; 
 }else if ((Average Pecentage<75) && (Average Pecentage >=70)){ 
 YourLetterGrade="C"; 
 }else if ((Average Pecentage<70) && (Average Pecentage >=65)){ 
 YourLetterGrade="D+"; 
 }else if ((Average Pecentage<65) && (Average Pecentage >=60)){ 
 YourLetterGrade="D"; 
 }else if (Average Pecentage<60){ 
 YourLetterGrade="F"; 
}
```
 
 All the methods that you use for writing the program should be in the form of functions

 It should have a function named about TheProgram() that is called immediately upon execution 
that alerts the user about the program and its functionality. 

To assist you with the project. I have written the function that reads data from a tab delimited 
file into an array and returns the number of rows in the file. You can compile the file using 
command line and use the example file provided to test the file. 

 You are also required to create a dummy students records file that is in the following format 
```text
Name   test1  test1  test3 
John   10     9      9 
Joe    9      10     6 
Mary   8      10     10 
Stella 10     7      9 
Maria  8      10     8 
Robert 5      10     7
```
The example file in the required format is attached. Your file should contain 5 tests and 10 
students. The maximum score for each test is 10. The average should be expressed in 
percentage before the grades are determined. The output should be Name, test1, test2,test3, 
test4, test5, testAverage, testAveragePercentage, Grade. If a test is dropped, then in the output 
it should be left blank. 
